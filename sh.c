#include <assert.h>
#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "list.h"

#define PERM		(0644)		/* default permission rw-r--r-- */
#define MAXBUF		(512)		/* max length of input line. */
#define MAX_ARG		(100)		/* max number of cmd line arguments. */
#define READBUF_SIZE (100)		/* size of input buffer when using read() */

typedef enum { 
	AMPERSAND, 			/* & */
	NEWLINE,			/* end of line reached. */
	NORMAL,				/* file name or command option. */
	INPUT,				/* input redirection (< file) */
	OUTPUT,				/* output redirection (> file) */
	PIPE,				/* | for instance: ls *.c | wc -l */
	SEMICOLON			/* ; */
} token_type_t;

static char*	progname;		/* name of this shell program. */
static char	input_buf[MAXBUF];	/* input is placed here. */
static char	token_buf[2 * MAXBUF];	/* tokens are placed here. */
static char*	input_char;		/* next character to check. */
static char*	token;			/* a token such as /bin/ls */

static list_t*	path_dir_list;		/* list of directories in PATH. */
static int	input_fd;		/* for i/o redirection or pipe. */
static int	output_fd;		/* for i/o redirection or pipe */

static char* oldwd; /* previous working directory */

/* fetch_line: read one line from user and put it in input_buf. */
int fetch_line(char* prompt)
{
	int	c;
	int	count;

	input_char = input_buf;
	token = token_buf;

	printf("%s", prompt);
	fflush(stdout);

	count = 0;

	for (;;) {

		c = getchar();

		if (c == EOF)
			return EOF;

		if (count < MAXBUF)
			input_buf[count++] = c;

		if (c == '\n' && count < MAXBUF) {
			input_buf[count] = 0;
			return count;
		}

		if (c == '\n') {
			printf("too long input line\n");
			return fetch_line(prompt);
		}

	}
}

/* end_of_token: true if character c is not part of previous token. */
static bool end_of_token(char c)
{
	switch (c) {
	case 0:
	case ' ':
	case '\t':
	case '\n':
	case ';':
	case '|':
	case '&':
	case '<':
	case '>':
		return true;

	default:
		return false;
	}
}

/* gettoken: read one token and let *outptr point to it. */
int gettoken(char** outptr)
{
	token_type_t	type;

	*outptr = token;

	while (*input_char == ' '|| *input_char == '\t')
		input_char++;

	*token++ = *input_char;

	switch (*input_char++) {
	case '\n':
		type = NEWLINE;
		break;

	case '<':
		type = INPUT;
		break;
	
	case '>':
		type = OUTPUT;
		break;
	
	case '&':
		type = AMPERSAND;
		break;
	
	case '|':
		type = PIPE; 
		break;

	case ';':
		type = SEMICOLON;
		break;
	
	default:
		type = NORMAL;

		while (!end_of_token(*input_char))
			*token++ = *input_char++;
	}

	*token++ = 0; /* null-terminate the string. */
	
	return type;
}

/* error: print error message using formatting string similar to printf. */
void error(char *fmt, ...)
{
	va_list		ap;

	fprintf(stderr, "%s: error: ", progname);

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	/* print system error code if errno is not zero. */
	if (errno != 0) {
		fprintf(stderr, ": ");
		perror(0);
	} else
		fputc('\n', stderr);

}

/* run_program: fork and exec a program. */
void run_program(char** argv, int argc, bool foreground, bool doing_pipe)
{
	/* you need to fork, search for the command in argv[0],
         * setup stdin and stdout of the child process, execv it.
         * the parent should sometimes wait and sometimes not wait for
	 * the child process (you must figure out when). if foreground
	 * is true then basically you should wait but when we are
	 * running a command in a pipe such as PROG1 | PROG2 you might
	 * not want to wait for each of PROG1 and PROG2...
	 * 
	 * hints:
	 *  snprintf is useful for constructing strings.
	 *  access is useful for checking wether a path refers to an 
	 *      executable program.
	 * 
	 * 
	 */
	if (strlen(argv[0]) == 2 && argv[0][0] == 'c' && argv[0][1] == 'd') {
		if (argc < 2) { // cd to home dir
			char* home = getenv("HOME");

			char* status = getcwd(oldwd, MAXBUF);
			if (status == NULL) {
				printf("Something went wrong\n");
			}

			if (chdir(home) != 0) {
				printf("This should not print\n");
			}
		} else if (argv[1][0] == '-') { // cd to previous dir
			char temp[MAXBUF];
			char* status = getcwd(temp, MAXBUF);
			if (status == NULL) {
				printf("Something went wrong\n");
			}
			if (chdir(oldwd) != 0) {
				printf("cd: oldwd not set\n");
			}
			memcpy(oldwd, temp, MAXBUF);
		} else { // cd to specified dir
			char* status = getcwd(oldwd, MAXBUF);
			if (status == NULL) {
				printf("Something went wrong\n");
			}
			if (chdir(argv[1]) != 0) {
				if (errno == EACCES) {
					printf("cd: %s access denied\n", argv[1]);
				} else if (errno == ENOENT) {
					printf("cd: %s does not exist\n", argv[1]);
				} else if (errno == ENOTDIR) {
					printf("cd: %s is not a directory\n", argv[1]);
				} else {
					printf("cd: Something went wrong\n");
				}
			}
		}
		return;
	}

	int pid = fork();
	if (pid == -1) { // Error
		printf("Something went wrong\n");
	} else if (pid == 0) { // Child process
		int file_exists = access(argv[0], F_OK);
		if (file_exists == 0) {
			int ok = access(argv[0], X_OK);
			if (ok == 0) {
				if (output_fd != 0) {
					if (dup2(output_fd, 1) == -1) {
						printf("This should not happen\n");
					}
				}
				if (input_fd != 0) {
					if (dup2(input_fd, 0) == -1) {
						printf("Something went wrong\n");
					}
				}
				if (execv(argv[0], argv) == -1) {
					printf("Execution of program failed");
				}
			} else {
				printf("%s: Permission denied\n", argv[0]);
			}
		} else {
			int i;
			list_t* entry = path_dir_list;
			for (i = 0; i < length(path_dir_list); i++) {
				int size = strlen(argv[0])+strlen((char*)entry->data)+2;
				char* buffer = malloc(size);
				snprintf(buffer, size, "%s/%s", (char*)entry->data, argv[0]);
				file_exists = access(buffer, F_OK);
				if (file_exists == 0) {
					int ok = access(buffer, X_OK);
					if (ok == 0) {
						argv[0] = buffer;
						if (output_fd != 0) {
							if (dup2(output_fd, 1) == -1) {
								printf("This should not happen\n");
							}
						}
						if (input_fd != 0) {
							if (dup2(input_fd, 0) == -1) {
								printf("Something went wrong\n");
							}
						}
						if (execv(buffer, argv) == -1) {
							printf("Execution of program failed");
						}
					} else {
						printf("%s: Permission denied\n", argv[0]);
					}
					i = -1;
					free(buffer);
					break;
				}
				free(buffer);
				entry = entry->succ;
			}
			if (i != -1) {
				printf("%s: Program does not exist\n", argv[0]);
			}
		}
		exit(0);
	} else { // Parent process
		if (output_fd != 0) {
			close(output_fd);
		}
		if (input_fd != 0) {
			close(input_fd);
		}
		if (foreground) {
			int wstatus;
			waitpid(pid, &wstatus, 0);
			if (!WIFEXITED(wstatus)) {
				printf("Something went wrong at exit of child\n");
			}
		}
	}
}

void parse_line(void)
{
	char*		argv[MAX_ARG + 1];
	int		argc;
	int		pipe_fd[2];	/* 1 for producer and 0 for consumer. */
	token_type_t	type;
	bool		foreground;
	bool		doing_pipe;

	input_fd	= 0;
	output_fd	= 0;
	argc		= 0;

	for (;;) {
			
		foreground	= true;
		doing_pipe	= false;

		type = gettoken(&argv[argc]);
		switch (type) {
		case NORMAL:
			argc += 1;
			break;

		case INPUT:
			type = gettoken(&argv[argc]);
			if (type != NORMAL) {
				error("expected file name: but found %s", 
					argv[argc]);
				return;
			}

			if (input_fd == 0) {
				input_fd = open(argv[argc], O_RDONLY);
			}

			if (input_fd < 0)
				error("cannot read from %s", argv[argc]);

			break;

		case OUTPUT:
			type = gettoken(&argv[argc]);
			if (type != NORMAL) {
				error("expected file name: but found %s", 
					argv[argc]);
				return;
			}

			output_fd = open(argv[argc], O_CREAT | O_WRONLY, PERM);

			if (output_fd < 0)
				error("cannot write to %s", argv[argc]);
			break;

		case PIPE:
			doing_pipe = true;

			if (pipe(pipe_fd) == -1) {
				printf("Something went wrong with the pipe\n");
			}

			/*FALLTHROUGH*/

		case AMPERSAND:
			foreground = false;

			/*FALLTHROUGH*/

		case NEWLINE:
		case SEMICOLON:

			if (argc == 0)
				return;
						
			argv[argc] = NULL;

			if (doing_pipe && output_fd == 0) {
				output_fd = pipe_fd[1];
			} else if (pipe_fd[1] != 0) {
				close(pipe_fd[1]);
				pipe_fd[1] = 0;
			}

			run_program(argv, argc, foreground, doing_pipe);

			input_fd	= 0;
			output_fd	= 0;
			argc		= 0;

			if (doing_pipe) {
				input_fd = pipe_fd[0];
			} else if (pipe_fd[0] != 0) {
				close(pipe_fd[0]);
				pipe_fd[0] = 0;
			}

			if (type == NEWLINE)
				return;

			break;
		}
	}
}

/* init_search_path: make a list of directories to look for programs in. */
static void init_search_path(void)
{
	char*		dir_start;
	char*		path;
	char*		s;
	list_t*		p;
	bool		proceed;

	path = getenv("PATH");

	/* path may look like "/bin:/usr/bin:/usr/local/bin" 
	 * and this function makes a list with strings 
	 * "/bin" "usr/bin" "usr/local/bin"
 	 *
	 */

	dir_start = malloc(1+strlen(path));
	if (dir_start == NULL) {
		error("out of memory.");
		exit(1);
	}

	strcpy(dir_start, path);

	path_dir_list = NULL;

	if (path == NULL || *path == 0) {
		path_dir_list = new_list("");
		return;
	}

	proceed = true;

	while (proceed) {
		s = dir_start;
		while (*s != ':' && *s != 0)
			s++;
		if (*s == ':')
			*s = 0;
		else
			proceed = false;

		insert_last(&path_dir_list, dir_start);

		dir_start = s + 1;
	}

	p = path_dir_list;

	if (p == NULL)
		return;

#if 0
	do {
		printf("%s\n", (char*)p->data);
		p = p->succ;	
	} while (p != path_dir_list);
#endif
}

/* main: main program of simple shell. */
int main(int argc, char** argv)
{
	progname = argv[0];
	oldwd = malloc(MAXBUF);

	init_search_path();	

	while (fetch_line("% ") != EOF)
		parse_line();

	free(oldwd);
	return 0;
}
